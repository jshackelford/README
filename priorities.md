## Weekly Priorities
This content is meant to communicate my priorities on a weekly basis. They should
typically be reflected as items in the `Doing` of
[my personal issue board](https://gitlab.com/groups/gitlab-com/-/boards/1702046?assignee_username=ebrinkman).
It will be updated weekly. I may also include a priority that is not work related,
work related but silly in nature, or a learning goal depending on the week.

### Legend
These designations are added to the previous week's priority list when adding the current week's priority.
* **Y** - Completed
* **N** - Not completed

## 2020-04-27
1. [Complete Forrester WSM Wave Process (Questionnaire, Survey, and Briefing)](https://gitlab.com/groups/gitlab-com/marketing/-/epics/806)
1. [Update the Dev Direction Page](https://gitlab.com/gitlab-com/Product/-/issues/1112)
1. [Prep and participate in Gartner IT Value Stream Inquiry](https://gitlab.com/gitlab-com/marketing/product-marketing/-/issues/2255)
1. [Align on Quality Management MVC](https://gitlab.com/gitlab-com/Product/-/issues/1113)
1. [Finalize Q1 OKR activites](https://gitlab.com/groups/gitlab-com/-/epics/262)
1. [Fun: Hold a Dev Section Pizza Party!](https://gitlab.com/gitlab-com/Product/-/issues/1114)